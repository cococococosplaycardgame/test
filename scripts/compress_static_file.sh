#/bin/sh

cd /usr/share/nginx/html/ && \
    find . -type f -name "*.js" -exec brotli -9kf {} \; -exec gzip -9kf {} \; && \
    find . -type f -name "*.css" -exec brotli -9kf {} \; -exec gzip -9kf {} \; && \
    find . -type f -name "*.svg" -exec brotli -9kf {} \; -exec gzip -9kf {} \; && \
    find . -type f -name "*.html" -exec brotli -9kf {} \; -exec gzip -9kf {} \; && \
    find . -type f -name "*.wasm" -exec brotli -9kf {} \; -exec gzip -9kf {} \;
